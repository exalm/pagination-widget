public class AppDrawer.Card : Gtk.EventBox {
	static construct {
		set_css_name ("card");
	}

	private double _aspect_ratio = 1;
	public double aspect_ratio {
		get { return _aspect_ratio; }
		set {
			_aspect_ratio = value;
			queue_resize ();
		}
	}

	// GtkWidget
	public override Gtk.SizeRequestMode get_request_mode () {
		return Gtk.SizeRequestMode.WIDTH_FOR_HEIGHT;
	}

	public override void get_preferred_height (out int minimum_height, out int natural_height) {
		measure (Gtk.Orientation.VERTICAL, -1, out minimum_height, out natural_height);
	}

	public override void get_preferred_width (out int minimum_width, out int natural_width) {
		measure (Gtk.Orientation.HORIZONTAL, -1, out minimum_width, out natural_width);
	}

	public override void get_preferred_height_for_width (int for_width, out int minimum_height, out int natural_height) {
		measure (Gtk.Orientation.VERTICAL, for_width, out minimum_height, out natural_height);
	}

	public override void get_preferred_width_for_height (int for_height, out int minimum_width, out int natural_width) {
		measure (Gtk.Orientation.HORIZONTAL, for_height, out minimum_width, out natural_width);
	}

	private void measure (Gtk.Orientation orientation, int for_size, out int minimum, out int natural) {
		minimum = 1;
		natural = 0;

		if (orientation == Gtk.Orientation.HORIZONTAL && for_size >= 0)
			natural = (int) (for_size * aspect_ratio);
		else
			natural = 1;
	}
}
