int main (string[] args) {
	typeof (AppDrawer.SamplePage).ensure ();
	typeof (AppDrawer.Favorites).ensure ();
	typeof (AppDrawer.Card).ensure ();

	var app = new Gtk.Application ("org.gnome.AppDrawer", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var screen = Gdk.Screen.get_default ();
		var provider = new Gtk.CssProvider ();
		provider.load_from_resource ("/org/gnome/AppDrawer/style.css");
		Gtk.StyleContext.add_provider_for_screen (screen, provider, 600);

		var win = app.active_window;
		if (win == null) {
			win = new AppDrawer.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}

