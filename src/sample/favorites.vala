[GtkTemplate (ui = "/org/gnome/AppDrawer/ui/favorites.ui")]
public class AppDrawer.Favorites : Gtk.Box {
	private static int COLUMNS = 4;

	construct {
		var all_apps = (new SampleData ()).all_apps;

		for (int i = 0; i < COLUMNS; i++) {
			var icon = new AppIcon (all_apps[i], true);
			add (icon);
		}
	}
}

