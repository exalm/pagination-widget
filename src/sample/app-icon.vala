[GtkTemplate (ui = "/org/gnome/AppDrawer/ui/app-icon.ui")]
public class AppDrawer.AppIcon : Gtk.Bin {
	[GtkChild]
	private Gtk.Image icon;
	[GtkChild]
	private Gtk.Label caption;

	public App app {
		construct {
			icon.icon_name = value.icon;
			caption.label = value.name;
		}
	}

	public bool is_favorite {
		construct {
			caption.visible = !value;
		}
	}

	public AppIcon (App app, bool is_favorite = false) {
		Object (app: app, is_favorite: is_favorite);
	}
}
