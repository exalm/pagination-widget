public class AppDrawer.SampleData {
	public const App[] all_apps = {
		{ "de.haeckerfelix.Fragments", "Fragments" },
		{ "im.bernard.Nostalgia",      "Nostalgia" },
		{ "org.gnome.ArchiveManager",  "Archive Manager" },
		{ "org.gnome.baobab",          "Disk Usage Analyzer" },
		{ "org.gnome.Books",           "Books" },
		{ "org.gnome.Boxes",           "Boxes" },
		{ "org.gnome.Builder",         "Builder" },
		{ "org.gnome.Calendar",        "Calendar" },
		{ "org.gnome.Characters",      "Characters" },
		{ "org.gnome.Cheese",          "Cheese" },
		{ "org.gnome.clocks",          "Clocks" },
		{ "org.gnome.Contacts",        "Contacts" },
		{ "org.gnome.Devhelp",         "Devhelp" },
		{ "org.gnome.DiskUtility",     "Disks" },
		{ "org.gnome.Documents",       "Documents" },
		{ "org.gnome.eog",             "Image Viewer" },
		{ "org.gnome.Epiphany",        "Web" },
		{ "org.gnome.Evince",          "Document Viewer" },
		{ "org.gnome.font-viewer",     "Fonts" },
		{ "org.gnome.Fractal",         "Fractal" },
		{ "org.gnome.Games",           "Games" },
		{ "org.gnome.Geary",           "Geary" },
		{ "org.gnome.gedit",           "Text Editor" },
		{ "org.gnome.Logs",            "Logs" },
		{ "org.gnome.Maps",            "Maps" },
		{ "org.gnome.Music",           "Music" },
		{ "org.gnome.Nautilus",        "Files" },
		{ "org.gnome.Photos",          "Photos" },
		{ "org.gnome.Polari",          "Polari" },
		{ "org.gnome.Screenshot",      "Screenshot" },
		{ "org.gnome.Settings",        "Settings" },
		{ "org.gnome.Software",        "Software" },
		{ "org.gnome.SoundRecorder",   "Sound Recorder" },
		{ "org.gnome.Sysprof",         "Sysprof" },
		{ "org.gnome.SystemMonitor",   "System Monitor" },
		{ "org.gnome.Terminal",        "Terminal" },
		{ "org.gnome.Totem",           "Videos" },
		{ "org.gnome.tweaks",          "Tweaks" },
		{ "org.gnome.Weather",         "Weather" },
		{ "org.gnome.Yelp",            "Help" },
	};
}

