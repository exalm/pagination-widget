#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PHOSH_TYPE_SWIPE_TRACKER (phosh_swipe_tracker_get_type())

G_DECLARE_FINAL_TYPE (PhoshSwipeTracker, phosh_swipe_tracker, PHOSH, SWIPE_TRACKER, GObject)

PhoshSwipeTracker *phosh_swipe_tracker_new (GtkWidget *widget);
gboolean           phosh_swipe_tracker_get_enabled (PhoshSwipeTracker *self);
void               phosh_swipe_tracker_set_enabled (PhoshSwipeTracker *self,
                                                    gboolean           enabled);
gboolean           phosh_swipe_tracker_get_reversed (PhoshSwipeTracker *self);
void               phosh_swipe_tracker_set_reversed (PhoshSwipeTracker *self,
                                                     gboolean           reversed);
gboolean           phosh_swipe_tracker_captured_event (PhoshSwipeTracker *self,
                                                       GdkEvent          *event);
void               phosh_swipe_tracker_confirm_swipe (PhoshSwipeTracker *self,
                                                      double             distance,
                                                      double            *snap_points,
                                                      int                n_snap_points,
                                                      double             current_progress,
                                                      double             cancel_progress);

G_END_DECLS
