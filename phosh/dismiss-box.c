#include "dismiss-box.h"

#include "swipe-tracker.h"

struct _PhoshDismissBox
{
  GtkEventBox parent_instance;

  struct {
    uint tick_cb_id;
    int64_t start_time;
    int64_t end_time;
    double start_position;
    double end_position;
  } animation_data;

  PhoshSwipeTracker *tracker;
  double progress;
};

G_DEFINE_TYPE (PhoshDismissBox, phosh_dismiss_box, GTK_TYPE_EVENT_BOX)

enum {
  PROP_0,
  PROP_INTERACTIVE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

enum {
  SIGNAL_DISMISSED,
  N_SIGNALS,
};

static guint signals [N_SIGNALS];

static void
set_progress (PhoshDismissBox *self,
              double           progress)
{
  self->progress = progress;

  if (progress <= -1 || progress >= 1)
    g_signal_emit (G_OBJECT (self), signals[SIGNAL_DISMISSED], 0);

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static double
ease_out_cubic (double t)
{
  double p = t - 1;
  return p * p * p + 1;
}

#define LERP(a, b, t) ((a) + (((b) - (a)) * (t)))

static gboolean
animation_cb (GtkWidget     *widget,
              GdkFrameClock *frame_clock,
              gpointer       user_data)
{
  PhoshDismissBox *self = PHOSH_DISMISS_BOX (widget);
  int64_t frame_time, duration;
  double position;
  double t;

  g_assert (self->animation_data.tick_cb_id != 0);

  frame_time = gdk_frame_clock_get_frame_time (frame_clock) / 1000;
  frame_time = MIN (frame_time, self->animation_data.end_time);

  duration = self->animation_data.end_time - self->animation_data.start_time;
  position = (double) (frame_time - self->animation_data.start_time) / duration;

  t = ease_out_cubic (position);
  set_progress (self, LERP (self->animation_data.start_position,
                            self->animation_data.end_position, t));

  if (frame_time == self->animation_data.end_time) {
    self->animation_data.tick_cb_id = 0;
    return G_SOURCE_REMOVE;
  }

  return G_SOURCE_CONTINUE;
}

static void
stop_animation (PhoshDismissBox *self)
{
  if (self->animation_data.tick_cb_id == 0)
    return;

  gtk_widget_remove_tick_callback (GTK_WIDGET (self),
                                   self->animation_data.tick_cb_id);
  self->animation_data.tick_cb_id = 0;
}

static void
animate (PhoshDismissBox *self,
         double           to,
         int64_t          duration)
{
  GdkFrameClock *frame_clock;
  int64_t frame_time;

  stop_animation (self);

  if (duration <= 0) {
    set_progress (self, to);
    return;
  }

  frame_clock = gtk_widget_get_frame_clock (GTK_WIDGET (self));
  frame_time = gdk_frame_clock_get_frame_time (frame_clock);

  self->animation_data.start_position = self->progress;
  self->animation_data.end_position = to;

  self->animation_data.start_time = frame_time / 1000;
  self->animation_data.end_time = self->animation_data.start_time + duration;
  self->animation_data.tick_cb_id =
    gtk_widget_add_tick_callback (GTK_WIDGET (self), animation_cb, self, NULL);
}

static void
swipe_begin_cb (PhoshDismissBox   *self,
                double             x,
                double             y,
                PhoshSwipeTracker *tracker)
{
  double distance;
  double *points;

  stop_animation (self);

  distance = 400;

  points = g_new (gdouble, 3);
  points[0] = -1;
  points[1] = 0;
  points[2] = 1;

  phosh_swipe_tracker_confirm_swipe (tracker, distance, points, 3,
                                     self->progress, 0);
}

static void
swipe_update_cb (PhoshDismissBox   *self,
                 double             value,
                 PhoshSwipeTracker *tracker)
{
  set_progress (self, value);
}

static void
swipe_end_cb (PhoshDismissBox   *self,
              gint64             duration,
              double             to,
              PhoshSwipeTracker *tracker)
{
  if (duration == 0) {
    set_progress (self, to);
    return;
  }

  animate (self, to, duration);
}

static gboolean
captured_event_cb (PhoshDismissBox *self,
                   GdkEvent        *event)
{
  return phosh_swipe_tracker_captured_event (self->tracker, event);
}

// GtkWidget

static gboolean
phosh_dismiss_box_draw (GtkWidget *widget,
                        cairo_t   *cr)
{
  PhoshDismissBox *self = PHOSH_DISMISS_BOX (widget);
  GtkWidget *child;

  child = gtk_bin_get_child (GTK_BIN (widget));
  if (!child)
    return GDK_EVENT_PROPAGATE;

  cairo_save (cr);

  cairo_translate (cr, 0, -self->progress * 400);
  gtk_container_propagate_draw (GTK_CONTAINER (widget), child, cr);

  cairo_restore (cr);

  return GDK_EVENT_PROPAGATE;
}

static void
phosh_dismiss_box_constructed (GObject *object)
{
  PhoshDismissBox *self = PHOSH_DISMISS_BOX (object);

  g_object_set (self->tracker, "orientation", GTK_ORIENTATION_VERTICAL, NULL);

  G_OBJECT_CLASS (phosh_dismiss_box_parent_class)->constructed (object);
}

static void
phosh_dismiss_box_finalize (GObject *object)
{
  PhoshDismissBox *self = (PhoshDismissBox *)object;

  g_signal_handlers_disconnect_by_data (self->tracker, self);
  g_object_unref (self->tracker);

  g_object_set_data (object, "captured-event-handler", NULL);

  G_OBJECT_CLASS (phosh_dismiss_box_parent_class)->finalize (object);
}

static void
phosh_dismiss_box_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  PhoshDismissBox *self = PHOSH_DISMISS_BOX (object);

  switch (prop_id) {
    case PROP_INTERACTIVE:
      g_value_set_boolean (value, phosh_dismiss_box_get_interactive (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
phosh_dismiss_box_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  PhoshDismissBox *self = PHOSH_DISMISS_BOX (object);

  switch (prop_id) {
    case PROP_INTERACTIVE:
      phosh_dismiss_box_set_interactive (self, g_value_get_boolean (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
phosh_dismiss_box_class_init (PhoshDismissBoxClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = phosh_dismiss_box_constructed;
  object_class->finalize = phosh_dismiss_box_finalize;
  object_class->get_property = phosh_dismiss_box_get_property;
  object_class->set_property = phosh_dismiss_box_set_property;
  widget_class->draw = phosh_dismiss_box_draw;

  properties [PROP_INTERACTIVE] =
    g_param_spec_boolean ("interactive",
                          "Interactive",
                          "Whether it can be swiped",
                          TRUE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals[SIGNAL_DISMISSED] =
    g_signal_new ("dismissed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  0);
}

static void
phosh_dismiss_box_init (PhoshDismissBox *self)
{
  self->progress = 0;

  self->tracker = phosh_swipe_tracker_new (GTK_WIDGET (self));
  g_signal_connect_swapped (self->tracker, "begin", G_CALLBACK (swipe_begin_cb), self);
  g_signal_connect_swapped (self->tracker, "update", G_CALLBACK (swipe_update_cb), self);
  g_signal_connect_swapped (self->tracker, "end", G_CALLBACK (swipe_end_cb), self);

  /*
   * HACK: GTK3 has no other way to get events on capture phase.
   * This is a reimplementation of _gtk_widget_set_captured_event_handler(),
   * which is private. In GTK4 it can be replaced with GtkEventControllerLegacy
   * with capture propagation phase
   */
  g_object_set_data (G_OBJECT (self), "captured-event-handler", captured_event_cb);
}

PhoshDismissBox *
phosh_dismiss_box_new (void)
{
  return g_object_new (PHOSH_TYPE_DISMISS_BOX, NULL);
}

gboolean
phosh_dismiss_box_get_interactive (PhoshDismissBox *self)
{
  g_return_val_if_fail (PHOSH_IS_DISMISS_BOX (self), FALSE);

  return phosh_swipe_tracker_get_enabled (self->tracker);
}

void
phosh_dismiss_box_set_interactive (PhoshDismissBox *self,
                                   gboolean         interactive)
{
  g_return_if_fail (PHOSH_IS_DISMISS_BOX (self));

  phosh_swipe_tracker_set_enabled (self->tracker, interactive);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_INTERACTIVE]);
}

